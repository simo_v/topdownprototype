﻿using UnityEngine;

public interface IDamageable {

	void TakeHit (float damage, GameObject dealer);
}

