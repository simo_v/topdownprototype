﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HitEffect : MonoBehaviour {
	ParticleSystem ps;
	Light flashLight;

	void Start() {
		ps = GetComponentInChildren<ParticleSystem> ();
		flashLight = GetComponentInChildren<Light> ();
	}
	void Update() {

		StartCoroutine (FlashLight ());
		if (ps) {
			if (!ps.IsAlive()) {
				Destroy (gameObject);
			}
		}
	}

	IEnumerator FlashLight() {
		yield return new WaitForSeconds(0.1f);
	
		flashLight.enabled = false;
	}
}
