﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DummyEnemy : DamageableBody {

	public GameObject focusLight;

	PlayerCharacter player;


	public override void Awake() {
		base.Awake ();

		player = FindObjectOfType (typeof(PlayerCharacter)) as PlayerCharacter;
	}

	void Update() {
		if (player.isInCombat) {
			if (player.GetLockedTarget ()) {
				if (player.GetLockedTarget () == this) {
					LightFocus (true);
				} else {
					LightFocus (false);
				}
			} else {
				LightFocus (false);
			}
		} else if (focusLight.activeSelf) {
			LightFocus (false);
		}
	}

	public override void TakeHit(float damage, GameObject dealer) {
		base.TakeHit(damage, dealer);

		//coriandoli
	}

	void LightFocus(bool value) {
		focusLight.SetActive (value);
	}
}
