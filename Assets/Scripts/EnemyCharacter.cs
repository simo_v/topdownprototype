﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent (typeof (Rigidbody))]
public class EnemyCharacter : Character{

	public GameObject focusLight;

	public EnemyBattleSphere battleSphere;

	PlayerCharacter player;


	public override void Awake() {
		base.Awake ();
		player = FindObjectOfType (typeof(PlayerCharacter)) as PlayerCharacter;
	}

	
	public override void Update() {

		base.Update ();

		//combat mode
		if (isInCombat) {
			currentVelocityMod = Vector3.MoveTowards (currentVelocityMod, velocity, acceleration * Time.deltaTime);
			moveVelocity = currentVelocityMod.normalized * combatMoveSpeed;

			isFocusedEnemyFar ();
			if (focusTarget) {
				targetRotation = Quaternion.LookRotation (focusTarget.transform.position - transform.position);
			}
		}

		//light the focus signal
		if (player.isInCombat) {
			if (player.GetLockedTarget ()) {
				if (player.GetLockedTarget () == this) {
					LightFocus (true);
				} else {
					LightFocus (false);
				}
			} else {
				LightFocus (false);
			}
		} else if (focusLight.activeSelf) {
			LightFocus (false);
		}
	}

	//tenere
	public void ToggleCombatMode() {
		isInCombat = !isInCombat;

		if (isInCombat) {
			animator.SetBool ("isInCombat", true);
			holdWeapon (true);
			AquireTarget ();
		} else {
			animator.SetBool ("isInCombat", false);
			holdWeapon (false);
		}
	}

	//enemy unique
	public void SetCombatMode(bool value) {
		isInCombat = value;

		if (isInCombat) {
			animator.SetBool ("isInCombat", true);
			holdWeapon (true);
			AquireTarget ();
		} else {
			animator.SetBool ("isInCombat", false);
			holdWeapon (false);
		}
	}

	void isFocusedEnemyFar() {
		if (!battleSphere.GetTargetsInRange().Contains(focusTarget)) {
			AquireTarget ();	
		}
	}

	//maneggiare
	void AquireTarget() {
		//serve catena di priorità

		List<DamageableBody> targets = battleSphere.GetTargetsInRange();

		if (targets.Count != 0) {
			foreach (DamageableBody item in targets) {
				if (focusTarget) {
					//if distance of the item is lower then the distance of the previous focusTarget TODO don't use Vector3.Distance since it is expensive with the square root
					if (Vector3.Distance (transform.position, item.transform.position) < Vector3.Distance (transform.position, focusTarget.transform.position)) {
						//make it the new focus target
						focusTarget = item;
					}
				} else {
					focusTarget = item;
				}
			}
		} else {
			focusTarget = null;
		}
		if (focusTarget && !lockedTarget) {
			lockedTarget = focusTarget;
		}
	}


	//enemy unique
	public IEnumerator StudyOpponent() {
		
		float reactionTime;

		if (currentState == State.standing) {
			//if too distant get closer
			while (lockedTarget != null) {
				reactionTime = Random.Range (0.5f, 1.5f);

				if (checkDistance (lockedTarget) == 1) {
					//muovi verso bersaglio
					Move ((lockedTarget.transform.position - transform.position).normalized);
				} else if (checkDistance (lockedTarget) == -1) {
					//muovi via dal bersaglio
					Move ((transform.position - lockedTarget.transform.position).normalized);

				} else if (checkDistance (lockedTarget) == 0) {
					//move = vector3.zero
					Move (Vector3.zero);
				}

				yield return new WaitForSeconds (reactionTime);
			}
		}
	}

	//enemy unique
	int checkDistance(DamageableBody body) {
		
		if (Vector3.Distance (transform.position, body.transform.position) > 2.5) {
			return 1;
		} else if (Vector3.Distance (transform.position, body.transform.position) < 2) {
			return -1;
		} else {
			return 0;
		}	
	}

	//enemy unique
	void LightFocus(bool value) {
		focusLight.SetActive (value);
	}
}
