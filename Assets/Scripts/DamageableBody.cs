﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DamageableBody : MonoBehaviour, IDamageable {

	public float startingHealth;
	protected float health;
	protected bool dead;

	public virtual void Awake() {
		health = startingHealth;
	}

	public virtual void TakeHit(float damage, GameObject dealer) {
		health -= damage;

		if (health <= 0 && !dead) {
			Die ();
		}	
	}

	public void Die() {
		dead = true;
		foreach (Renderer item in GetComponentsInChildren<Renderer>()) {
			item.enabled = false;
		}
		foreach (Collider item in GetComponentsInChildren<Collider>()) {
			item.enabled = false;
		}
		foreach (Rigidbody item in GetComponents<Rigidbody>()) {
			item.useGravity = false;
		}
		Destroy (gameObject, 1);
	}

	public bool GetIsDead() {
		return dead;
	}
}
