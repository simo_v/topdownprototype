﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent (typeof (PlayerCharacter))]
public class PlayerController : MonoBehaviour {

	private float nextAttackTime = 0;
	PlayerCharacter playChar;

	public float attackSpeed = 1.5f;


	// Use this for initialization
	void Start () {
		playChar = GetComponent<PlayerCharacter> ();
	}
	
	// Update is called once per frame
	void Update () {

		/*** directional input ***/
		if (playChar.GetState ().Equals ("standing")) {
			Vector3 moveInput = new Vector3 (Input.GetAxisRaw ("Horizontal"), 0, Input.GetAxisRaw ("Vertical"));
			playChar.Move (moveInput.normalized);
		} else {
			playChar.Move (Vector3.zero);
		}

		/*** combat mode input ***/
		if (Input.GetButtonDown ("Combat mode")) {
			playChar.ToggleCombatMode ();
		}


		/*** attack input ***/
		if (playChar.isInCombat) {
			if (Time.time > nextAttackTime && playChar.GetState().Equals("standing")) {
				if (Input.GetButtonDown ("Attack")) {
					playChar.StartCoroutine (playChar.Attack ());
					nextAttackTime = Time.time + attackSpeed;
				}
			}
		}


		/*** defend input ***/
		if (playChar.isInCombat) {
			if (Input.GetButton ("Defend")  && playChar.GetState().Equals("standing")) {
				playChar.Defend (true);
				//playChar.Move (Vector3.zero);
			} else if(Input.GetButtonUp("Defend") && playChar.GetState().Equals("defend")){
				playChar.Defend (false);
			}
		}
	}
}
