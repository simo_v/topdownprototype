﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent (typeof (Rigidbody))]
public class PlayerCharacter : Character {

	public PlayerBattleSphere battleSphere;
	
	public override void Move(Vector3 velocityValue) {
		base.Move (velocityValue);
		LockTarget (velocityValue.normalized);
	}


	public override void Update() {

		if (focusTarget) {
			if (focusTarget.GetIsDead ()) {
				battleSphere.GetTargetsInRange ().Remove (focusTarget);
				focusTarget = null;
			}
		}
		if (lockedTarget) {
			if (lockedTarget.GetIsDead ()) {
				battleSphere.GetTargetsInRange ().Remove (lockedTarget);
				lockedTarget = null;
			}
		}

		base.Update ();

		//combat mode
		if (isInCombat) {
			currentVelocityMod = Vector3.MoveTowards (currentVelocityMod, velocity, acceleration * Time.deltaTime);
			moveVelocity = currentVelocityMod.normalized * combatMoveSpeed;

			IsFocusedEnemyFar ();
			if (focusTarget) {
				targetRotation = Quaternion.LookRotation (focusTarget.transform.position - transform.position);
			}

			//find and draw the lock target region
			vertexB = transform.position + lockTargetDirection * focusLength + Vector3.Cross(lockTargetDirection,Vector3.up) * findCatFocus();
			vertexC = transform.position + lockTargetDirection * focusLength - Vector3.Cross(lockTargetDirection,Vector3.up) * findCatFocus();

			Debug.DrawRay (transform.position, vertexB - transform.position, Color.yellow);
			Debug.DrawRay (transform.position, vertexC - transform.position, Color.yellow);
			Debug.DrawRay (vertexB, vertexC-vertexB, Color.yellow);

			//lock a target
			foreach (DamageableBody item in battleSphere.GetTargetsInRange()) {
				if (!item.GetIsDead ()) {
					if (checkIfLockableTarget (item.transform.position)) {
						if (lockedTarget) {
							if (checkIfLockableTarget (lockedTarget.transform.position)) {
								if (Vector3.Distance (transform.position, lockedTarget.transform.position) > Vector3.Distance (transform.position, item.transform.position)) {
									lockedTarget = item;
								}
							} else {
								lockedTarget = item;
							}
						} else {
							lockedTarget = item;
						}
					}
				}
			}
			if (battleSphere.GetTargetsInRange ().Count == 0) {
				lockedTarget = null;
			}
		}
	}

	public void ToggleCombatMode() {
		isInCombat = !isInCombat;

		if (isInCombat) {
			animator.SetBool ("isInCombat", true);
			holdWeapon (true);
			AquireTarget ();
		} else {
			animator.SetBool ("isInCombat", false);
			holdWeapon (false);
		}
	}
		
	/************* checks if lockedTarget is in range of the attack *********************/

	public bool checkIfLockableTarget(Vector3 point) {

		double s1 = vertexC.z - transform.position.z;
		double s2 = vertexC.x - transform.position.x;
		double s3 = vertexB.z - transform.position.z;
		double s4 = point.z - transform.position.z;

		double w1 = (transform.position.x * s1 + s4 * s2 - point.x * s1) / (s3 * s2 - (vertexB.x - transform.position.x) * s1);
		double w2 = (s4 - w1 * s3) / s1;

		return w1 >= 0 && w2 >= 0 && (w1 + w2) <= 1;
	}
		
	float findIpoFocus() {
		float ipo = focusLength / Mathf.Cos (focusWidth/2);
		return ipo;
	}

	float findCatFocus() {
		float cat = Mathf.Sqrt (Mathf.Pow (findIpoFocus(), 2) - Mathf.Pow (focusLength, 2));
		return cat;
	}

	void IsFocusedEnemyFar() {
		if (!battleSphere.GetTargetsInRange().Contains(focusTarget)) {
			AquireTarget ();	
		}
	}
		
	void AquireTarget() {
		//serve catena di priorità

		List<DamageableBody> targets = battleSphere.GetTargetsInRange();

		if (targets.Count != 0) {
			foreach (DamageableBody item in targets) {
				if (focusTarget) {
					//if distance of the item is lower then the distance of the previous focusTarget TODO don't use Vector3.Distance since it is expencive with the square root
					if (Vector3.Distance (transform.position, item.transform.position) < Vector3.Distance (transform.position, focusTarget.transform.position)) {
						//make it the new focus target
						focusTarget = item;
					}
				} else {
					focusTarget = item;
				}
			}
		} else {
			focusTarget = null;
		}
		if (focusTarget && !lockedTarget) {
			lockedTarget = focusTarget;
		}
	}
}
