﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponCollider : MonoBehaviour {

	public GameObject hitEffect;

	void OnTriggerEnter(Collider col) {
		if (col.GetComponent<DamageableBody> ()) {
			col.GetComponent<DamageableBody> ().TakeHit (1, GetComponentInParent<Character>().gameObject);
		}
	}
}
