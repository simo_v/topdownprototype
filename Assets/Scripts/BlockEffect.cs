﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlockEffect : MonoBehaviour {
	Light flashLight;

	void Start() {
		flashLight = GetComponentInChildren<Light> ();
	}
	void Update() {

		StartCoroutine (FlashLight ());

	}

	IEnumerator FlashLight() {
		yield return new WaitForSeconds(0.06f);
	
		flashLight.enabled = false;

		Destroy (this);
	}
}
