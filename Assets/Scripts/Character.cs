﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent (typeof (Rigidbody))]
public class Character : DamageableBody {

	protected enum State {attacking, standing, hit, defend

	}
	protected State currentState;

	protected Quaternion targetRotation;
	protected Vector3 moveVelocity;
	protected Vector3 currentVelocityMod;
	protected DamageableBody focusTarget;
	protected DamageableBody lockedTarget;
	protected Vector3 lockTargetDirection;

	[HideInInspector]
	public bool isInCombat = false;

	public float focusLength = 2;
	public float focusWidth = 45;
	public float acceleration = 5;
	public float moveSpeed = 5;
	public float combatMoveSpeed = 5;
	public float rotationSpeed = 450;

	protected Vector3 velocity;
	protected Rigidbody myRigidbody;
	protected Animator animator;
	public GameObject hitEffect;
	public GameObject blockEffect;
	public GameObject sheath;
	public GameObject itemEquipped;

	//debug
	protected Vector3 vertexB;
	protected Vector3 vertexC;

	// Use this for initialization
	public override void Awake () {

		base.Awake ();

		myRigidbody = GetComponent<Rigidbody> ();
		animator = GetComponentInChildren<Animator> ();
		itemEquipped.SetActive (false);
		currentState = State.standing;
	}

	public virtual void Move(Vector3 velocityValue) {
		velocity = velocityValue;
	
		animator.SetFloat ("Speed", velocityValue.magnitude);

		if (velocityValue != Vector3.zero && focusTarget == null) {
			Rotate (velocityValue.normalized);
		}
	}

	public void Rotate(Vector3 directionValue) {
		targetRotation = Quaternion.LookRotation (directionValue);
	}


	public void LockTarget(Vector3 directionValue) {
		if (directionValue != Vector3.zero) {
			lockTargetDirection = directionValue;
		} else {
			lockTargetDirection = transform.forward;
		}
	}

	public virtual void Update() {

		//normal mode
		if (!isInCombat) {
			currentVelocityMod = Vector3.MoveTowards (currentVelocityMod, velocity, acceleration * Time.deltaTime);
			moveVelocity = currentVelocityMod.normalized * moveSpeed;

			focusTarget = null;
		} 
	}

	public void FixedUpdate() {

		if (currentState == State.standing) {
			myRigidbody.MovePosition (myRigidbody.position + moveVelocity * Time.fixedDeltaTime);

			transform.eulerAngles = Vector3.up * Mathf.MoveTowardsAngle (transform.localEulerAngles.y, targetRotation.eulerAngles.y, rotationSpeed * Time.fixedDeltaTime);
		}
	}

	public void holdWeapon(bool value) {
		sheath.SetActive (!value);
		itemEquipped.SetActive (value);
	}


	public IEnumerator Attack() {

		Vector3 targetLeap;
		float percent = 0;

		currentState = State.attacking;

		transform.rotation = targetRotation;

		if (!lockedTarget) {
			transform.rotation = targetRotation;
			targetLeap = transform.position + transform.forward;
		} else {
			transform.rotation = Quaternion.LookRotation (lockedTarget.transform.position - transform.position);
			targetLeap = lockedTarget.transform.position + (transform.position - lockedTarget.transform.position).normalized / 1.2f;
			focusTarget = lockedTarget;
		}
			
		animator.SetTrigger ("Attack");

		while (percent <= 1) {

			percent += Time.deltaTime;
			transform.position = Vector3.Lerp (transform.position, targetLeap, percent);
			yield return null;
		}

		yield return new WaitForSeconds (0.4f);
		currentState = State.standing;
	}

	public string GetState() {
		return currentState.ToString ();
	}

	public void SetState(string value) {
		currentState = (State)System.Enum.Parse(typeof(State), value);
	}
		
	public DamageableBody GetLockedTarget() {
		return lockedTarget;
	}

	public override void TakeHit (float damage, GameObject dealer){
		transform.rotation = Quaternion.LookRotation(dealer.transform.position - transform.position);

		if (dealer.GetComponent<Character> ()) {
			focusTarget = dealer.GetComponent<DamageableBody> ();
		}

		if (currentState != State.defend) {
			base.TakeHit (damage, dealer);

			StartCoroutine (GettingHit ());
		} else {
			StartCoroutine (BlockHit ());
		}

	}

	public IEnumerator GettingHit() {
		Vector3 targetFall;
		float percent = 0;

		Instantiate (hitEffect, transform.position, Quaternion.LookRotation(Vector3.forward));

		currentState = State.hit;

		targetFall = transform.position - (transform.forward * 1.5f);

		animator.SetTrigger ("Damage");

		while (percent <= 1) {

			percent += Time.deltaTime * 3;
			transform.position = Vector3.Lerp (transform.position, targetFall, percent);
			yield return null;
		}

		yield return new WaitForSeconds (0.6f);
		currentState = State.standing;

	}

	public IEnumerator BlockHit() {
		Vector3 targetFall;
		float percent = 0;

		Instantiate (blockEffect, transform.position, Quaternion.LookRotation(Vector3.forward));

		targetFall = transform.position - (transform.forward);

		while (percent <= 1) {

			percent += Time.deltaTime * 3;
			transform.position = Vector3.Lerp (transform.position, targetFall, percent);
			yield return null;
		}

	}

	public void Defend(bool value) {
		if (value) {
			currentState = State.defend;
		} else {
			Rotate (transform.forward);
			currentState = State.standing;
		}
		animator.SetBool ("Defend", value);
	}
}
