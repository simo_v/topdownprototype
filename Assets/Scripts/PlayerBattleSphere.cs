﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerBattleSphere : BattleSphere {

	void OnTriggerEnter(Collider col) {
		if (col.GetComponent<DamageableBody> () && !col.GetComponent<PlayerCharacter>() && !col.GetComponent<DamageableBody>().GetIsDead()) {
			DamageableBody enemy = col.GetComponent<DamageableBody> ();
			GetTargetsInRange().Add (enemy);
		}
	}
}
