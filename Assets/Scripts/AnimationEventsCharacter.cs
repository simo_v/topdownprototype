﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationEventsCharacter : MonoBehaviour {

	public GameObject weaponCol;
	public GameObject weaponEffect;

	private Character owner;

	void Awake() {
		owner = GetComponentInParent<Character> ();
	}

	void Update() {
		if (!owner.GetState ().Equals ("attacking") || owner.GetIsDead()) {
			weaponCol.SetActive (false);
			weaponEffect.SetActive (false);
		}
	}

	void ActivateWeaponCollider() {
		weaponCol.SetActive (true);
		weaponEffect.SetActive (true);
	}

	void DeactivateWeaponCollider() {
		weaponCol.SetActive (false);
		weaponEffect.SetActive (false);
	}
}
