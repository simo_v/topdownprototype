﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BattleSphere : MonoBehaviour {

	List<DamageableBody> targetsInRange = new List<DamageableBody>();

	public virtual void OnTriggerExit(Collider other) {
		targetsInRange.Remove (other.GetComponent<DamageableBody> ());
	}

	public List<DamageableBody> GetTargetsInRange() {
		return targetsInRange;
	}

	public void RemoveFromList(DamageableBody character) {
		targetsInRange.Remove(character);
	}
}
