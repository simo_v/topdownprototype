﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyBattleSphere : BattleSphere {

	void OnTriggerEnter(Collider col) {
		if (col.GetComponent<DamageableBody> () && col.GetComponent<PlayerCharacter>() && !col.GetComponent<DamageableBody>().GetIsDead()) {
			DamageableBody enemy = col.GetComponent<DamageableBody> ();
			GetTargetsInRange().Add (enemy);
		}
	}

	public override void OnTriggerExit(Collider other) {
		base.OnTriggerExit (other);

		//vedere se fare una funzione per enemy controller in modo che non guardi ad ogni frame se non ha nemici nella sphere
	}
}
