﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;


[RequireComponent (typeof (NavMeshAgent))]
public class EnemyController : MonoBehaviour {

	private float decideToAttackTime;
	private float decideToDefendTime;
	private float nextAttackTime = 0;
	public float attackSpeed = 1;

	enum State{chasing, fighting

		}
	State currentState;

	NavMeshAgent pathfinder;
	Transform target;
	EnemyBattleSphere battleSphere;
	EnemyCharacter enemyChar;

	// Use this for initialization
	void Awake () {
		pathfinder = GetComponent<NavMeshAgent> ();
		target = GameObject.FindGameObjectWithTag ("Player").transform;
		battleSphere = GetComponentInChildren<EnemyBattleSphere> ();
		enemyChar = GetComponent<EnemyCharacter> ();
		decideToAttackTime = Random.Range (2, 10);
		decideToDefendTime = Random.Range (3, 15);

		currentState = State.chasing;
	}
	
	// Update is called once per frame
	void Update () {
		if (currentState == State.chasing) {

			if (!pathfinder.isActiveAndEnabled) {
				pathfinder.enabled = true;
			}

			enemyChar.SetCombatMode(false);

			StartCoroutine (UpdatePath ());

			if (battleSphere.GetTargetsInRange ().Count > 0) {
				if (currentState != State.fighting) {
					currentState = State.fighting;
					enemyChar.Move (Vector3.zero);
				}
			}
		} else if (currentState == State.fighting) {
			enemyChar.SetCombatMode(true);

			if (pathfinder.isActiveAndEnabled) {
				pathfinder.Stop ();
				pathfinder.ResetPath ();
				pathfinder.enabled = false;
			}

			enemyChar.StartCoroutine (enemyChar.StudyOpponent ());

			if (!enemyChar.GetLockedTarget ().GetComponent<PlayerCharacter> ().isInCombat) {
				if (enemyChar.GetState ().Equals("standing")) {
					AttackCommand ();
				}
			} else {
				DecideToAttack ();
				DecideToDefend ();
			}

			if (enemyChar.GetLockedTarget ().GetComponent<PlayerCharacter> ().GetState ().Equals ("attacking")) {
				StartCoroutine (DefendCommandWithReaction ());
			}

			if (battleSphere.GetTargetsInRange ().Count <= 0) {
				if (enemyChar.GetLockedTarget () && !enemyChar.GetLockedTarget ().GetComponent<Character> ().isInCombat) {
					currentState = State.chasing;
				}
			}
		}
	}


	//recalculate path every 0.25 seconds instead of every frame for performance issues
	IEnumerator UpdatePath() {
		float refreshRate = .25f;

		pathfinder.ResetPath ();

		pathfinder.Resume ();

		enemyChar.Move (pathfinder.velocity.normalized);

		while (target != null && currentState != State.fighting && pathfinder.isActiveAndEnabled) {
			Vector3 targetPosition = new Vector3 (target.position.x, 0, target.position.z);
			pathfinder.SetDestination (targetPosition);

			yield return new WaitForSeconds (refreshRate);

		}
	}


	void AttackCommand() {
		if (Time.time > nextAttackTime) {
			if (enemyChar.GetState ().Equals ("standing")) {
				enemyChar.StartCoroutine (enemyChar.Attack ());
			}
			nextAttackTime = Time.time + attackSpeed;
		}
	}


	void DecideToAttack() {
		if (Time.time > decideToAttackTime) {
			if (enemyChar.GetState ().Equals("standing")) {
				AttackCommand ();
			}
			decideToAttackTime = Time.time + Random.Range (2, 10);
		}
	}

	void DecideToDefend() {

		if (Time.time > decideToDefendTime) {
			if (enemyChar.GetState ().Equals ("standing")) {
				StartCoroutine(DefendCommandWithReaction ());
			}
		}
		decideToDefendTime = Time.time + Random.Range (3, 15);


	}

	IEnumerator DefendCommandWithReaction() {
		yield return new WaitForSeconds(Random.Range(0,2.5f));

		if (enemyChar.GetState ().Equals ("hit")) {
			yield return null; 
		}

		else if (enemyChar.GetState ().Equals ("standing")) {

			float counterAttack = Random.Range (0, 100);

			enemyChar.Defend (true);

			if (counterAttack > 50) {

				yield return new WaitForSeconds (1f);
				enemyChar.Defend (false);

				AttackCommand ();
			} else {

				yield return new WaitForSeconds (Random.Range (2, 3));

				enemyChar.Defend (false);
			}
		}
	}
}
